/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dnj3dSensor;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author dima
 */
public class FXMLLogController implements Initializable {

    public FXMLDocumentController mainCtrl;
    
    @FXML
    private TextArea textLog;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void Add(String text){
        String s = textLog.getText();
        
        if(s.length()>3000) s="";
        
        textLog.setText(s+text+"\n");
        textLog.positionCaret(textLog.getText().length());
    }
    
}
