/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dnj3dSensor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

/**
 *
 * @author dima
 */
public class FXMLDocumentController implements Initializable {

    private Boolean isRSSend=false;
    static SerialPort serialPort;
    @FXML
    private CheckBox cbCrc8;
    @FXML
    private CheckBox cbTimerRun;
    
    @FXML
    private CheckBox cbSeries1,cbSeries2,cbSeries3;
    
    @FXML
    private ComboBox<String> portsList;
    
    @FXML
    private ComboBox<String> cbDataType;
    
    @FXML
    private Button btnOpen;
    @FXML
    private Button btnClose;
    
    @FXML
    private Button btnCmd;
           
    @FXML
    private TextField editCmd;
    @FXML
    private ComboBox<String> portsSpeed;
        
    @FXML
    private NumberAxis xAxis;
    @FXML
    private NumberAxis yAxis;
    
    @FXML
    private final XYChart.Series series1 = new XYChart.Series();
    @FXML
    private final XYChart.Series series2 = new XYChart.Series();
    @FXML
    private final XYChart.Series series3 = new XYChart.Series();
    @FXML
    private LineChart<Integer, Integer>  lineChart;
    
    public Stage stngStage = null;
    public Stage logStage = null;
    private FXMLLogController logCtrl;
    
    public Timer timer;
    public TimerTask task;
    
    public void ComState(){
        boolean en = ((serialPort!=null) && (serialPort.isOpened()));
        
        editCmd.setDisable(!en);
        cbTimerRun.setDisable(!en);
        btnClose.setDisable(!en);        
        btnCmd.setDisable(!en);
        cbDataType.setDisable(!en);
        
        btnOpen.setDisable(en);        
        portsSpeed.setDisable(en);
        portsList.setDisable(en);
        
        if(!en){
            if(task!=null){
                task.cancel();
            }
        }
    }
    
    @FXML
    private void handleButtonAction(ActionEvent event) {       
        findPorts(); 
    }
    
    /**
    * Поиск доступных портов
    *
    */
    @FXML
    public void findPorts(){
        portsList.getItems().clear();    
        String[] portNames = SerialPortList.getPortNames();        
        portsList.getItems().addAll(Arrays.asList(portNames));
        
        ComState();
        if(portsList.getItems().isEmpty()){
            btnLogForm(null);
            LogAdd("Нет доступных портов");
            LogAdd("Для Ubuntu");
            LogAdd("sudo chmod 666 /dev/ttyUSB0 - дать доступ до перезагрузки");
            LogAdd("sudo usermod -a -G dialout $USER - дабавить пользователя в группу");            
        }
    }
    
    @FXML
    private void btnOpenAction(ActionEvent event) {
        if(serialPort==null){
            serialPort = new SerialPort("");
        }
        if(task!=null){
            task.cancel();
        }
        cbTimerRun.setSelected(false);
        
        if((serialPort!=null) && (!serialPort.isOpened()) && (portsList.getValue() != null)){
            serialPort = new SerialPort(portsList.getValue());
            try {
                serialPort.openPort();//Open serial port
                serialPort.setParams(Integer.parseInt(portsSpeed.getValue()), 
                    SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            }
            catch (SerialPortException ex) {
                //System.out.println(ex);
                btnLogForm(null);
                LogAdd("Ошибка порта: "+ex.getMessage());
            }
        }
        ComState();
    }
    @FXML
    private void btnCloseAction(ActionEvent event) {              
        if(task!=null){
            task.cancel();
        }
        cbTimerRun.setSelected(false);
        
        if((serialPort!=null) && (serialPort.isOpened())){
            try{
                serialPort.closePort();
            }
            catch (SerialPortException ex) {System.out.println(ex);}
        }      
        ComState();
    }
    @FXML
    private void btnLogForm(ActionEvent event) {
        //! this should open the other form, but it doesn't
        //Parent root;
        try {
            FXMLLoader root = new FXMLLoader(SensSetting.class.getResource("FXMLLog.fxml"));
            AnchorPane page = (AnchorPane) root.load();
            //root = FXMLLoader.load(getClass().getResource("FXMLSettings.fxml"));
            Scene scene = new Scene(page, 360, 480);
            if(logStage==null){
                logStage = new Stage();
                logStage.setTitle("Лог");
                logStage.setScene(scene);
                
                logCtrl = root.getController();
                logCtrl.mainCtrl = this;
            }
                        
            logStage.show();
            logStage.toFront();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
    * Запись в окно лога
    *
    * @param text   Текст
    */
    public void LogAdd(String text){
        if(logStage!=null){
            logCtrl.Add(text);
        }            
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }  
    
    @FXML
    public void init(){
        series1.setName("1 Байт");
        series2.setName("2 Инт1");
        series3.setName("3 Инт2");
        lineChart.getData().addAll(series1, series2, series3);
        
        findPorts();
        loadParams();
    }
    @FXML
    private void cbSeriesVisible(ActionEvent event){
        //cbSeries1,cbSeries2,cbSeries3
        //lineChart.getData().
        
        lineChart.getData().removeAll(series1,series2,series3);
        if(cbSeries1.isSelected()){
            lineChart.getData().add(series1);
        }
        
        if(cbSeries2.isSelected()){
            lineChart.getData().add(series2);
        }
        
        if(cbSeries3.isSelected()){
            lineChart.getData().add(series3);
        }
    }    

    @FXML
    private void btnStngForm(ActionEvent event){
        //! this should open the other form, but it doesn't
        //Parent root;
        try {
/*            FXMLLoader root = new FXMLLoader(SensSetting.class.getResource("FXMLDocument.fxml"));
            AnchorPane page = (AnchorPane) root.load();

            Scene scene = new Scene(page);        
            stage.setScene(scene);
            stage.show();

            ctrl = root.getController();
*/
            FXMLLoader root = new FXMLLoader(SensSetting.class.getResource("FXMLSettings.fxml"));
            AnchorPane page = (AnchorPane) root.load();
            //root = FXMLLoader.load(getClass().getResource("FXMLSettings.fxml"));
            Scene scene = new Scene(page, 360, 630);
            if(stngStage==null){
                stngStage = new Stage();
                stngStage.setTitle("Настройки");
                stngStage.setScene(scene);
                
                FXMLSettingsController ctrl = root.getController();
                //ctrl.serialPort = serialPort;
                ctrl.mainCtrl = this;
            }
            
            
            stngStage.show();
            stngStage.toFront();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    private void btnCmdAction(ActionEvent event){
        //Отправка произвольной команды
        byte[] b = hexStringToByteArray(editCmd.getText());
        byte[] b2 = sendCmd(b,1000);   
        if(b2!=null){
            String s =bytesToHex(b2);
            JOptionPane.showMessageDialog(null, s, "Ответ", JOptionPane.INFORMATION_MESSAGE);
        }
        
        /*
        if((serialPort!=null) && (serialPort.isOpened())){ 
            System.out.println(editCmd.getText());
            
            byte[] b = hexStringToByteArray(editCmd.getText());
            if(cbCrc8.isSelected()){
                byte crc = (byte)CRC8.compute(b);              
                byte[] b2 = Arrays.copyOf(b, b.length + 1);
                b2[b2.length - 1] = crc;                 
                b=b2;
            }            
           
            try{                
                LogAdd("Send: "+bytesToHex(b));
                serialPort.writeBytes(b);
                
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                String s =serialPort.readHexString();
                JOptionPane.showMessageDialog(null, s, "Ответ", JOptionPane.INFORMATION_MESSAGE);
                LogAdd("Read: "+s);
                
            }
            catch (SerialPortException ex) {System.out.println(ex);}
        }
        else{
            JOptionPane.showMessageDialog(null,"Ком порт закрыт!", "Инфо", JOptionPane.INFORMATION_MESSAGE);
        }
        */
    }
    @FXML
    private void cbDataTypeAction(ActionEvent event){        
        series1.getData().clear();
        series2.getData().clear();
        series3.getData().clear();
    }
    @FXML
    private void cbTimerRunAction(ActionEvent event){
        if(timer==null){
            timer = new java.util.Timer();
        }
               
        if(cbTimerRun.isSelected()){
            //task.
            task = new TimerTask() {
                @Override
                public void run() {
                     Platform.runLater(() -> {
                         if("DYP-ME007".equals(cbDataType.getValue())){
                             graphDypME007();
                         }
                         else{
                             graphOmnicom();                         
                         }
                     });
                }
            };
            
            //task.
            
            timer.schedule(task, 500,500);
        }
        else{
            task.cancel();
        }
    }
    
    private void graphDypME007(){
        try{
            if(serialPort.getInputBufferBytesCount()>=4){
                byte[] b =serialPort.readBytes();
                //System.out.println("Read: "+bytesToHex(b));
                LogAdd("Read: "+bytesToHex(b));
                
                //Парсинг
                int start=-1;
                for(int i=0;i<b.length-1;i++){
                    if(((b[i] & 0xFF) == 0xFF)){//&&((b[i+1] & 0xFE) != 0xFE)){
                        start=i;                        
                    }
                    
                    if((start>=0) && ((start+4) <= b.length)){
                        //System.out.println("start: "+start+" length: "+b.length);
                        int p = ((b[start+1]<<8) & 0xFF00) | (b[start+2] & 0xFF);
                          
                        addSeries(p,-1,-1);
                        
                        i+=3;
                        start=-1;
                    }
                }
                
                
            }
        }
        catch (SerialPortException ex) {
            System.out.println(ex);
            btnLogForm(null);
            LogAdd("Ошибка порта: "+ex.getMessage());
        }
    }
    private void graphOmnicom(){
        byte[] b = hexStringToByteArray("31"+"01"+cbDataType.getValue());
        System.out.println("Send: "+bytesToHex(b));
        //LogAdd("Send: "+bytesToHex(b));
        b = sendCmd(b,400);
        if(b!=null){
            System.out.println("Read: "+bytesToHex(b));
            //LogAdd("Read: "+bytesToHex(b));
            byte crc = (byte)CRC8.compute(b, 0, b.length-1);
            if(crc == b[b.length-1]){
                //знач: 1-реверс 0-норма
                //dirVector1.setValue(dirVector1.getItems().get(b[3]));
                int p1=b[3],p2=getInt(b,4),p3=getInt(b,6);
                addSeries(p1,p2,p3);                                 
            }
        }
    }
    
    private void addSeries(int p1,int p2,int p3){
        LogAdd("Chart: "+p1+" "+p2+" "+p3);
                                 
        if(series1.getData().size() > 200){
            series1.getData().clear(); //.remove(0, series1.getData().size()-99);
        }
        if(p1!=-1)
        series1.getData().add(new XYChart.Data(series1.getData().size()+1, p1));
                                 
        if(series2.getData().size() > 200){
            series2.getData().clear();
        }
        if(p2!=-1)
        series2.getData().add(new XYChart.Data(series2.getData().size()+1, p2));
                
        if(series3.getData().size() > 200){
            series3.getData().clear();
        }
        if(p3!=-1)
        series3.getData().add(new XYChart.Data(series3.getData().size()+1, p3));
    }
    
    /**
    * Отправка команды в порт, с добавлением в конец контрольной суммы
    * и ожиданием ответа с таймаутом равным timeout.
    * Если команда не прошла или нет ответа то вернет null
    *
    * @param data Команда для отправки
    * @param timeout Время ожидания ответа, в милисекундах
    * @return byte[]
    */
    public byte[] sendCmd(byte[] data, int timeout){  
        if((serialPort!=null) && (serialPort.isOpened())){
            //Проверка что порт свободный
            //isRSSend
            
            Date tmp_timeout;// = new Date();
            /*while(isRSSend){
                try {Thread.sleep(65);
                    //System.out.println("wait");
                } catch (InterruptedException ex) {break;}   
                if(((new Date()).getTime() - tmp_timeout.getTime()) >= 3000)break; 
            }*/
            
            if(!isRSSend){
                isRSSend=true;
                byte crc = (byte)CRC8.compute(data);              
                byte[] b2 = Arrays.copyOf(data, data.length + 1);
                b2[b2.length - 1] = crc;
                try{
                    LogAdd("Send: "+bytesToHex(b2));
                    serialPort.writeBytes(b2);
                    //Ожадание ответа
                    tmp_timeout = new Date();
                    try {

                        while(true){
                            Thread.sleep(50);                                             
                            if(((new Date()).getTime() - tmp_timeout.getTime()) >= timeout)break;                            
                            if(serialPort.getInputBufferBytesCount()>=9){break;}
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                        LogAdd("Ошибка ожидания ответа: "+ex.getMessage());
                    }

                    b2 =serialPort.readBytes();
                    if(b2!=null){
                        if(b2.length>0){
                            LogAdd("Send: "+bytesToHex(b2));
                        }
                        if(data[2] != b2[2]){
                            LogAdd("Ошибка ответа!");
                        }      
                        //Проверка crc ответа
                        crc = (byte)CRC8.compute(b2, 0, b2.length-1);
                        if(crc != b2[b2.length-1]){
                            LogAdd("Ошибка контрольной суммы ответа!");
                            isRSSend=false;
                            return null;
                        }
                    }
                    isRSSend=false;
                    return b2;
                }
                catch (SerialPortException ex) {
                    System.out.println(ex);
                    btnLogForm(null);
                    LogAdd("Ошибка порта: "+ex.getMessage());
                }
                isRSSend=false;
            }
        }       
        return null;
    }
    public byte[] sendCmd(byte[] data){         
        return sendCmd(data, 700);
    }
    /**
    * Возвращает 2байтовое чисто по протоколу Omnicom из масива с указанной позиции
    *
    * @param bytes Масив данных
    * @param offset Позиция начала
    * @return int
    */
    public static int getInt(byte[] bytes,int offset) {
        int res = ((bytes[offset+1] << 8) & 0xFF00);
        res += (bytes[offset] & 0xFF);        
        return res;
    }
    /**
    * Проверка строки
    *
    * @param str
    * @return boolean
    */
    public static boolean isInt(String str){  
        try  
        {  
          int d = Integer.parseInt(str);  
        }  
        catch(NumberFormatException nfe)  
        {  
          return false;  
        }  
        return true;  
    }
    
    /**
    * Преобразование строки HEX в масив байтов
    *
    * @param s
    * @return byte[]
    */
    public static byte[] hexStringToByteArray(String s) {
    byte[] b = new byte[s.length() / 2];
    String hh="";
    for (int i = 0; i < b.length; i++) {
      int index = i * 2;
      int v = Integer.parseInt(s.substring(index, index + 2), 16);
      
      hh+=" "+v;
      
      b[i] = (byte) v;
    }
    //System.out.println(hh);    
    return b;
  }
    /**
    * Преобразование масива байтов в строку HEX
    *
    * @param bytes
    * @return String
    */
    public static String bytesToHex(byte[] bytes) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for ( int j = 0; j < bytes.length; j++ ) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
    
    
    public void saveParamChanges() {
        try {
            Properties props = new Properties();
            props.setProperty("Port", portsList.getValue());
            props.setProperty("Speed", portsSpeed.getValue());
            File f = new File("server.properties");
            OutputStream out = new FileOutputStream( f );
            props.store(out, "This is an optional header comment string");
        }
        catch (Exception e ) {
            e.printStackTrace();
        }
    }
    public void loadParams() {
        Properties props = new Properties();
        InputStream is;
        // First try loading from the current directory
        try {
            File f = new File("server.properties");
            is = new FileInputStream( f );
        }
        catch ( Exception e ) { is = null; }

        try {
            if ( is == null ) {
                // Try loading from classpath
                is = getClass().getResourceAsStream("server.properties");
            }
            // Try loading properties from the file (if found)
            props.load( is );
        }
        catch ( Exception e ) { }

        portsList.setValue(props.getProperty("Port", ""));
        portsSpeed.setValue(props.getProperty("Speed", "19200"));
    }
}
