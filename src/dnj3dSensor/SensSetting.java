/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dnj3dSensor;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author dima
 */
public class SensSetting extends Application {
    
    private FXMLDocumentController ctrl;
    
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader root = new FXMLLoader(SensSetting.class.getResource("FXMLDocument.fxml"));
        AnchorPane page = (AnchorPane) root.load();
                
        Scene scene = new Scene(page);        
        stage.setScene(scene);
        stage.setTitle("3D sensor ["+System.getProperty("os.name")+" "+System.getProperty("os.version")+"]");
        stage.show();        
        
        ctrl = root.getController();
        ctrl.init();                
        //Событие - закрытия формы
        stage.setOnCloseRequest((WindowEvent we) -> {
            if(ctrl.stngStage!=null){ctrl.stngStage.close();}
            if(ctrl.logStage!=null){ctrl.logStage.close();}
            if(ctrl.task!=null){ctrl.task.cancel();}
            if(ctrl.timer!=null){ctrl.timer.cancel();}
            ctrl.saveParamChanges();
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
