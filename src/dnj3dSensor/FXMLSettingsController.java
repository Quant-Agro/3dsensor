/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dnj3dSensor;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;
import static dnj3dSensor.FXMLDocumentController.bytesToHex;
import static dnj3dSensor.FXMLDocumentController.hexStringToByteArray;
import static dnj3dSensor.FXMLDocumentController.isInt;
import static dnj3dSensor.FXMLDocumentController.getInt;

/**
 * FXML Controller class
 *
 * @author dima
 */
public class FXMLSettingsController implements Initializable {
    //public SerialPort serialPort;
    public FXMLDocumentController mainCtrl;
    
    @FXML
    private TextField devID,devDiv,devFac,devOff,devMin,devMax;
    
    @FXML
    private ComboBox<String> dirVector1,dirVector2;
    @FXML
    private ComboBox<String> activAxis1,activAxis2;
    @FXML
    private ComboBox<String> orientAxis1,orientAxis2;
    @FXML
    private CheckBox revertVal;
    
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
        
    
    @FXML
    private void btnCmd3233(ActionEvent event){
        //Направление вектора установка        
        ObservableList<String> list = dirVector1.getItems();        
        byte val = (byte)list.indexOf(dirVector1.getValue());
        
        byte[] b = hexStringToByteArray("31"+devID.getText()+"3200"+String.format("%02X ", val));
        System.out.println("Send: "+bytesToHex(b));
        mainCtrl.sendCmd(b);
                
        list = dirVector2.getItems();        
        val = (byte)list.indexOf(dirVector2.getValue());        
        b = hexStringToByteArray("31"+devID.getText()+"3300"+String.format("%02X ", val));
        System.out.println("Send: "+bytesToHex(b));
        mainCtrl.sendCmd(b);

    }
    @FXML
    private void btnCmd2122(ActionEvent event){
        //Направление вектора читать
        //первый параметр - направление вектора          
        byte[] b = hexStringToByteArray("31"+devID.getText()+"21");
        System.out.println("Send: "+bytesToHex(b));
        b = mainCtrl.sendCmd(b);      
        if(b!=null){
            System.out.println("Read: "+bytesToHex(b));
            byte crc = (byte)CRC8.compute(b, 0, b.length-1);
            if(crc == b[b.length-1]){
                //знач: 1-реверс 0-норма
                dirVector1.setValue(dirVector1.getItems().get(b[3]));
            }
        }
        //
        b = hexStringToByteArray("31"+devID.getText()+"22");
        System.out.println("Send: "+bytesToHex(b));
        b = mainCtrl.sendCmd(b);   
        if(b!=null){
            System.out.println("Read: "+bytesToHex(b));
            byte crc = (byte)CRC8.compute(b, 0, b.length-1);
            if(crc == b[b.length-1]){
                //знач: 1-реверс 0-норма
                dirVector2.setValue(dirVector2.getItems().get(b[3]));
            }
        }
    }
       
    
    @FXML
    private void btnCmd4142(ActionEvent event){
        //Ось установка
        ObservableList<String> list = activAxis1.getItems();        
        byte val = (byte)list.indexOf(activAxis1.getValue());
        byte[] b = hexStringToByteArray("31"+devID.getText()+"4100"+String.format("%02X ", val));
        System.out.println("Send: "+bytesToHex(b));
        mainCtrl.sendCmd(b);    
        
        list = activAxis2.getItems();        
        val = (byte)list.indexOf(activAxis2.getValue());
        b = hexStringToByteArray("31"+devID.getText()+"4200"+String.format("%02X ", val));
        System.out.println("Send: "+bytesToHex(b));
        mainCtrl.sendCmd(b);
    }
    @FXML
    private void btnCmd40(ActionEvent event){
        //Ось читать
        
        byte[] b = hexStringToByteArray("31"+devID.getText()+"40");
        System.out.println("Send: "+bytesToHex(b));
        b = mainCtrl.sendCmd(b);   
        if(b!=null){
            System.out.println("Read: "+bytesToHex(b));
            byte crc = (byte)CRC8.compute(b, 0, b.length-1);        
            //System.out.println(String.format("%02X ", crc));
            if(crc == b[b.length-1]){
                //3E 01 40 00 0100 0100 46                
                //знач: 0-x 1-y 2-z
                activAxis1.setValue(activAxis1.getItems().get(b[4]));
                activAxis2.setValue(activAxis2.getItems().get(b[6]));                
            }
        }
    }
    
    @FXML
    private void btnCmd4647(ActionEvent event){
        //Ось ориентация установка
        ObservableList<String> list = orientAxis1.getItems();        
        byte val = (byte)list.indexOf(orientAxis1.getValue());
        byte[] b = hexStringToByteArray("31"+devID.getText()+"4600"+String.format("%02X ", val));
        mainCtrl.sendCmd(b);    
        
        list = orientAxis2.getItems();        
        val = (byte)list.indexOf(orientAxis2.getValue());
        b = hexStringToByteArray("31"+devID.getText()+"4700"+String.format("%02X ", val));
        mainCtrl.sendCmd(b);
    }
    @FXML
    private void btnCmd45(ActionEvent event){
        //Ось ориентация читать
        byte[] b = hexStringToByteArray("31"+devID.getText()+"45");
        b = mainCtrl.sendCmd(b);   
        if(b!=null){
            byte crc = (byte)CRC8.compute(b, 0, b.length-1);
            if(crc == b[b.length-1]){
                //3E 01 45 00 0000 0000 E6
                //знач: 1-реверс 0-норма
                orientAxis1.setValue(orientAxis1.getItems().get(b[4]));
                orientAxis2.setValue(orientAxis2.getItems().get(b[6]));
            }
        }
    }
    @FXML
    private void btnCmd48(ActionEvent event){        
        //Отрицательные знач. читать
        //
        byte[] b = hexStringToByteArray("31"+devID.getText()+"48");
        b = mainCtrl.sendCmd(b);   
        if(b!=null){
            byte crc = (byte)CRC8.compute(b, 0, b.length-1);   
            if(crc == b[b.length-1]){
                //3E 01 48 00 0100 0000 23
                //знач: 1-реверс 0-норма
                revertVal.setSelected(b[4]==1);
            }
        }
    }
        
    @FXML
    private void btnCmd49(ActionEvent event){
        //Отрицательные знач. установка             
        byte val = (byte)(revertVal.isSelected() ? 1 : 0);
        byte[] b = hexStringToByteArray("31"+devID.getText()+"4900"+String.format("%02X ", val));
        mainCtrl.sendCmd(b);  
    }
    
    @FXML
    private void btnCmd03(ActionEvent event){
        //установка адреса
             
        //byte val = (byte)(dev.isSelected() ? 1 : 0);
        //byte[] b = hexStringToByteArray("31"+devID.getText()+"03"+String.format("%02X ", val));
        //System.out.println("Send: "+bytesToHex(b));
        //sendCmd(b);  
    }
    
    @FXML
    private void btnCmdCC(ActionEvent event){
        //команда на принудительную перезагрузку датчика
        byte[] b = hexStringToByteArray("31"+devID.getText()+"CC");
        mainCtrl.sendCmd(b);  
    }
    @FXML
    private void btnCmd30(ActionEvent event){
        //кооманда на установку значения Zero
        byte[] b = hexStringToByteArray("31"+devID.getText()+"30");
        mainCtrl.sendCmd(b);  
    }
    @FXML
    private void btnCmd31(ActionEvent event){
        //изменение суммирующего значения на противоположное
        byte[] b = hexStringToByteArray("31"+devID.getText()+"31");
        mainCtrl.sendCmd(b); 
    }
    
    @FXML
    private void btnCmd54(ActionEvent event){
        //отладка
        byte[] b = hexStringToByteArray("31"+devID.getText()+"54");
        mainCtrl.sendCmd(b); 
    }
    
    @FXML
    private void btnCmd1011(ActionEvent event){
        //читать калибровки

        byte[] b = hexStringToByteArray("31"+devID.getText()+"10");
        b = mainCtrl.sendCmd(b);   
        if(b!=null){
            byte crc = (byte)CRC8.compute(b, 0, b.length-1);      
            if(crc == b[b.length-1]){
                devDiv.setText(b[3]+"");
                devFac.setText(getInt(b,4)+"");
                devOff.setText(getInt(b,6)+"");
            }
        }
        
        b = hexStringToByteArray("31"+devID.getText()+"11");
        b = mainCtrl.sendCmd(b);   
        if(b!=null){
            byte crc = (byte)CRC8.compute(b, 0, b.length-1);   
            if(crc == b[b.length-1]){
                devMin.setText(getInt(b,4)+"");
                devMax.setText(getInt(b,6)+"");
            }
        }
    }
    
    @FXML
    private void btnCmd1213(ActionEvent event){
        //записать калибровки
        if(!isInt(devDiv.getText())){
            JOptionPane.showMessageDialog(null, "Значение '"+devDiv.getText()+"' не целое число!", "Ответ", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(!isInt(devFac.getText())){
            JOptionPane.showMessageDialog(null, "Значение '"+devFac.getText()+"' не целое число!", "Ответ", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(!isInt(devOff.getText())){
            JOptionPane.showMessageDialog(null, "Значение '"+devOff.getText()+"' не целое число!", "Ответ", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(!isInt(devMin.getText())){
            JOptionPane.showMessageDialog(null, "Значение '"+devMin.getText()+"' не целое число!", "Ответ", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(!isInt(devMax.getText())){
            JOptionPane.showMessageDialog(null, "Значение '"+devMax.getText()+"' не целое число!", "Ответ", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        //
        int val = Integer.parseInt(devDiv.getText());
        String cmd=String.format("%02X", val & 0xFF);
        
        val = Integer.parseInt(devFac.getText());
        cmd+=String.format("%02X", val & 0xFF) + String.format("%02X", (val>>8) & 0xFF);
        
        val = Integer.parseInt(devOff.getText());
        cmd+=String.format("%02X", val & 0xFF) + String.format("%02X", (val>>8) & 0xFF);

        byte[] b = hexStringToByteArray("31"+devID.getText()+"12"+cmd);
        mainCtrl.sendCmd(b);    
        
        ////////////////////////////////////////////////////////////////////////
        val = Integer.parseInt(devMin.getText());
        cmd=String.format("%02X", val & 0xFF) + String.format("%02X", (val>>8) & 0xFF);
        
        val = Integer.parseInt(devMax.getText());
        cmd+=String.format("%02X", val & 0xFF) + String.format("%02X", (val>>8) & 0xFF);

        b = hexStringToByteArray("31"+devID.getText()+"1300"+cmd);
        mainCtrl.sendCmd(b);    
    }

    
}
